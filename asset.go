package ingest

import (
	"sync"
	"time"

	"github.com/satori/go.uuid"
)

// Format ffprobe format metadata
type Format struct {
	NbStreams      uint              `json:"nb_streams"`
	FormatName     string            `json:"format_name"`
	FormatLongName string            `json:"format_long_name"`
	StartTime      string            `json:"start_time"`
	Duration       string            `json:"duration"`
	Size           string            `json:"size"`
	BitRate        string            `json:"bit_rate"`
	Tags           map[string]string `json:"tags"`
}

// Stream ffprobe stream metadata
type Stream struct {
	Index         uint    `json:"index,omitempty"`
	CodecName     string  `json:"codec_name,omitempty"`
	CodecLongName string  `json:"codec_long_name,omitempty"`
	CodecType     string  `json:"codec_type,omitempty"`
	Profile       string  `json:"profile,omitempty"`
	Width         uint    `json:"width,omitempty"`
	Height        uint    `json:"height,omitempty"`
	Duration      string  `json:"duration,omitempty"`
	DurationTs    float64 `json:"duration_ts,omitempty"`
}

// Metadata ffprobe metadata
type Metadata struct {
	Streams []*Stream `json:"streams"`
	Format  *Format   `json:"format"`
}

// Asset describes a media asset
type Asset struct {
	ID        *uuid.UUID `json:"id,omitempty"`
	Title     string     `json:"title,omitempty"`
	File      string     `json:"file,omitempty"`
	Size      int64      `json:"size,omitempty"`
	Mtime     time.Time  `json:"mtime,omitempty"`
	Hash      string     `json:"hash,omitempty"`
	Metadata  *Metadata  `json:"metadata,omitempty"`
	MimeType  string     `json:"mimeType,omitempty"`
	URL       string     `json:"url,omitempty"`
	Poster    string     `json:"poster,omitempty"`
	Thumbnail string     `json:"thumbnail,omitempty"`
	sync.Mutex
}
