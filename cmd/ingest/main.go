package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os/user"
	"path/filepath"
	"sync"

	"gitlab.com/mediat/ingest"
)

func expand(path string) (string, error) {
	if len(path) == 0 || path[0] != '~' {
		return path, nil
	}

	usr, err := user.Current()
	if err != nil {
		return "", err
	}
	return filepath.Join(usr.HomeDir, path[1:]), nil
}

var (
	uploadFlag    bool
	notifyURLFlag string
	notifyURL     *url.URL
)

func handleURL(u, notify *url.URL) (err error) {
	if u.Scheme == "" {
		u.Scheme = "file"
	}
	if u.Scheme == "file" {
		expanded, _ := expand(u.Path)
		u.Path, err = filepath.Abs(expanded)
		if err != nil {
			return
		}
	}
	log.Print("Ingesting ", u)
	asset, err := ingest.Ingest(u)
	if err != nil {
		return fmt.Errorf("Failed to ingest '%s'; %s", u, err)
	}
	enc, err := json.Marshal(asset)
	if err != nil {
		return fmt.Errorf("Failed to marshal asset: %s", err)
	}

	r, err := http.Post(notify.String(), "application/json", bytes.NewBuffer(enc))
	if err != nil {
		return fmt.Errorf("Failed to upload asset data: %s", err)
	}
	if r.StatusCode != http.StatusOK {
		return fmt.Errorf("Failed to upload asset data: %s", r.Status)
	}
	log.Println("Done with", u)
	return
}

func main() {
	flag.StringVar(&notifyURLFlag, "notify-url", "http://localhost:8000/api/asset", "Where to POST asset information, eg http://localhost:8000/api/asset")
	flag.BoolVar(&uploadFlag, "upload", false, "Perform upload of local file")

	flag.Parse()

	if flag.NArg() < 1 {
		log.Fatalln("No filename was provided")
	}

	notifyURL, err := url.Parse(notifyURLFlag)
	if err != nil {
		log.Fatalln(err)
	}
	if uploadFlag {
		notifyURL.Query().Set("upload", "true")
	}

	var wg sync.WaitGroup
	urls := make(chan *url.URL)
	numworkers := 4
	wg.Add(numworkers)
	for i := 1; i <= numworkers; i++ {
		go func() {
			defer wg.Done()
			for u := range urls {
				err := handleURL(u, notifyURL)
				if err != nil {
					log.Println(err)
				}
			}
		}()
	}
	for _, a := range flag.Args() {
		u, err := url.Parse(a)
		if err != nil {
			log.Println(err)
			continue
		}
		urls <- u
	}
	close(urls)
	wg.Wait()
	return
}
