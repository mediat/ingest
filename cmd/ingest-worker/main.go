package main

import (
	"encoding/json"
	"log"
	"net/url"
	"time"

	"gitlab.com/mediat/ingest"
)

var (
	ingestQueue = "ingest:file"
)

func main() {
	client := ingest.NewRedisClient()
	defer client.Close()
	for {
		time.Sleep(time.Second * 2)
		log.Println("Ingestworker waiting")
		result, err := client.BLPop(0, ingestQueue).Result()
		if err != nil {
			log.Println("BLPop failed", err)
			continue
		}
		log.Println("Got file", result[1])
		u, err := url.Parse(result[1])
		if err != nil {
			log.Println(err)
			continue
		}
		if u.Scheme == "" {
			u.Scheme = "file"
		}
		asset, err := ingest.Ingest(u)
		if err != nil {
			log.Println("Failed to ingest", err)
			continue
		}
		encodedAsset, err := json.Marshal(asset)
		if err != nil {
			log.Println("Failed to marshal info", err)
			continue
		}
		log.Println("Publishing info for", u)
		client.Publish(ingest.AssetAddCommand, encodedAsset)
	}
}
