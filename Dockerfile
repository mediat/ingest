FROM golang:1.9.2

WORKDIR /go/src/gitlab.com/mediat/ingest

RUN apt-get update && apt-get -qqy install ffmpeg

ADD . .

RUN go-wrapper download
RUN go-wrapper install ./...

CMD ["go-wrapper", "run"]