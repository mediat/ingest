package ingest

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sts"
)

func identity(s *session.Session) (string, error) {
	stsSvc := sts.New(s)
	result, err := stsSvc.GetCallerIdentity(&sts.GetCallerIdentityInput{})
	if err != nil {
		return "", err
	}
	return *result.Account, nil
}

// AwsSession "standard" aws session
func AwsSession() *session.Session {
	return session.Must(session.NewSessionWithOptions(session.Options{
		Config: aws.Config{
			Region: aws.String(AwsConstants.Region),
		},
	}))
}

type constants struct {
	Profile    string
	Endpoint   string
	BucketName string
	Region     string
}

// Constants
var (
	AwsConstants = &constants{
		"wka", "rgw.wka.se", "mediat", "us-east-1",
	}
)

// RgwSession openstack session
func RgwSession() *session.Session {
	return session.Must(session.NewSessionWithOptions(session.Options{
		Config: aws.Config{
			Endpoint:         aws.String(AwsConstants.Endpoint),
			S3ForcePathStyle: aws.Bool(true),
			Region:           aws.String(AwsConstants.Region),
		},
		Profile: AwsConstants.Profile,
	}))
}
