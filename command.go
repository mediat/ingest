package ingest

// Commands
const (
	AssetUpdateCommand = "ingest:AssetUpdateCommand"
	AssetAddCommand    = "ingest:AssetAddCommand"
	AssetUploadCommand = "ingest:AssetUploadCommand"
)
