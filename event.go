package ingest

import (
	"encoding/json"
	"log"

	"github.com/go-redis/redis"
)

// Constants
const (
	AssetUploadedEvent = "ingest:AssetUploadedEvent"

	redisURL = "redis://localhost:6379/0"
)

var (
	publisher *redis.Client
	pubsub    *redis.PubSub
	handlers  map[string][]interface{}
)

func init() {
	publisher = NewRedisClient()
	pubsub = publisher.Subscribe()
}

// NewRedisClient create default client
func NewRedisClient() *redis.Client {
	opt, err := redis.ParseURL(redisURL)
	if err != nil {
		panic(err)
	}
	return redis.NewClient(opt)
}

func On(channel string, handler func()) {
	pubsub.Subscribe(channel)
}

func Off(channel string) {
	pubsub.Unsubscribe(channel)
}

func Emit(channel string, payload interface{}) {
	go func(ch string, p interface{}) {
		encoded, err := json.Marshal(payload)
		if err != nil {
			log.Println(err)
			return
		}
		publisher.Publish(channel, encoded)
	}(channel, payload)
}
