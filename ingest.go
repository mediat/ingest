package ingest

import (
	"bytes"
	"crypto/md5"
	"encoding/json"
	"fmt"
	"io"
	"mime"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"path"
	"strings"
	"sync"
	"time"
)

func titleFromPath(p string) string {
	return strings.TrimSuffix(path.Base(p), path.Ext(p))
}

func assetFromFile(file string) (*Asset, error) {
	stat, err := os.Lstat(file)
	if err != nil {
		return nil, err
	}
	mimeType := mime.TypeByExtension(path.Ext(file))
	if len(mimeType) == 0 {
		mimeType = "application/octet-stream"
	}
	return &Asset{
		Title:    titleFromPath(file),
		File:     file,
		Size:     stat.Size(),
		Mtime:    stat.ModTime(),
		MimeType: mimeType,
	}, nil
}

func assetFromHTTP(u *url.URL) (*Asset, error) {
	r, err := http.Head(u.String())
	if err != nil {
		return nil, err
	}
	mtime, err := time.Parse(time.RFC1123, r.Header.Get("Last-Modified"))
	if err != nil {
		mtime = time.Unix(0, 0)
	}
	etag := r.Header.Get("ETag")
	if len(etag) > 0 {
		etag = strings.Trim(etag, "\"")
	}
	return &Asset{
		Title:    titleFromPath(u.Path),
		Size:     r.ContentLength,
		Mtime:    mtime,
		MimeType: r.Header.Get("Content-Type"),
		URL:      u.String(),
		Hash:     etag,
	}, nil
}

func newAsset(u *url.URL) (asset *Asset, err error) {
	switch u.Scheme {
	case "file":
		return assetFromFile(u.Path)
	default:
		return assetFromHTTP(u)
	}
}

func fileHash(file string) (hash string, err error) {
	f, err := os.Open(file)
	if err != nil {
		return
	}
	defer f.Close()
	h := md5.New()
	if _, err = io.Copy(h, f); err != nil {
		return
	}
	hash = fmt.Sprintf("%x", h.Sum(nil))
	return
}

func metadata(u *url.URL) (data *Metadata, err error) {
	cmd := exec.Command("ffprobe", "-v", "error", "-print_format", "json", "-show_streams", "-show_format", u.String())
	var out bytes.Buffer
	cmd.Stdout = &out
	err = cmd.Run()
	if err != nil {
		return nil, err
	}
	if err = json.Unmarshal(out.Bytes(), &data); err != nil {
		return nil, err
	}
	return data, nil
}

// Ingest harvest information from media file
func Ingest(u *url.URL) (asset *Asset, err error) {
	file := u.Path
	if asset, err = newAsset(u); err != nil {
		return
	}
	done := make(chan bool, 1)
	fail := make(chan error, 1)
	var (
		wg sync.WaitGroup
	)
	wg.Add(1)
	go func() {
		defer wg.Done()
		md, err := metadata(u)
		if err != nil {
			fail <- err
			return
		}
		asset.Lock()
		defer asset.Unlock()
		asset.Metadata = md
	}()
	if u.Scheme == "file" {
		wg.Add(1)
		go func() {
			defer wg.Done()
			hash, err := fileHash(file)
			if err != nil {
				fail <- err
				return
			}
			asset.Lock()
			defer asset.Unlock()
			asset.Hash = hash
		}()
	}
	go func() {
		wg.Wait()
		close(done)
	}()
	select {
	case <-done:
	case err = <-fail:
	}
	return
}
